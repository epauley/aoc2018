ops = "addr addi mulr muli banr bani borr bori setr seti gtir gtri gtrr eqri eqir eqrr".split()

def addr(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a]+regs[b]
    

def addi(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a]+b
    

def mulr(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a]*regs[b]
    

def muli(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a]*b
    
    
def banr(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a] & regs[b]
    
    
def bani(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a] & b
    

def borr(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a] | regs[b]
    
    
def bori(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a] | b
    
    
def setr(regs,opcode):
    id,a,b,c = opcode
    regs[c] = regs[a]
    
    
def seti(regs,opcode):
    id,a,b,c = opcode
    regs[c] = a
    
    
def gtir(regs,opcode):
    id,a,b,c = opcode
    regs[c] = int(a > regs[b])
    
    
def gtri(regs,opcode):
    id,a,b,c = opcode
    regs[c] = int(regs[a] > b)
    
    
def gtrr(regs,opcode):
    id,a,b,c = opcode
    regs[c] = int(regs[a] > regs[b])
    
    
def eqir(regs,opcode):
    id,a,b,c = opcode
    regs[c] = int(a == regs[b])
    
    
def eqri(regs,opcode):
    id,a,b,c = opcode
    regs[c] = int(regs[a] == b)
    
    
def eqrr(regs,opcode):
    id,a,b,c = opcode
    regs[c] = int(regs[a] == regs[b])
    

funcs = [   (addr,"addr"),
            (addi,"addi"),
            (mulr,"mulr"),
            (muli,"muli"),
            (banr,"banr"),
            (bani,"bani"),
            (borr,"borr"),
            (bori,"bori"),
            (setr,"setr"),
            (seti,"seti"),
            (gtri,"gtri"),
            (gtrr,"gtrr"),
            (gtir,"gtir"),
            (eqrr,"eqrr"),
            (eqri,"eqri"),
            (eqir,"eqir")
        ]
        
funcDict = {}
for v,k in funcs:
    funcDict[k] = v
    
inp = open("input.txt","r").read().strip().split("\n")

ipReg = int(inp[0].split("#ip ")[1])
prog = []
for line in inp[1:]:
    instr,a,b,c = line.split()
    prog.append((instr,int(a),int(b),int(c)))

#program loop
regs = [1,0,0,0,0,0]
#regs = [4, 10551267, 10551267, 8, 0, 5-1]
t = 0
while True:
    ip = regs[ipReg]
    if(not (0 <= ip < len(prog))):
        print("IP @",ip,"out of bounds, halting")
        print(regs)
        print(t)
        break
        
    #if(t % 10000000 < 100):
    if(True):
        print(t,"IP @",ip,"Executing instruction",prog[ip])
        print("\tRegs Before",regs)
        funcDict[prog[ip][0]](regs,prog[ip])
        print("\tRegs After",regs)
    else:
        funcDict[prog[ip][0]](regs,prog[ip])
    input()
    regs[ipReg] += 1
    t += 1
