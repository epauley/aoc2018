#include <stdio.h>

int main() {
    unsigned long a,b,c,d,Z,bTarget;
    a = 0;
    b = 0;
    c = 0;
    d = 0;
    Z = 1;
    
    //Part 1 Setup
    a += 2;
    a *= a;
    a *= 19;
    a *= 11;
    d += 1;
    d *= 22;
    d += 9;
    a += d;
    
    //a = 867;
    //d = 31;
    
    //Part 2 Setup
    if(Z == 1) {
        d = 5;
        d *= 28;
        d += 29;
        d *= 30;
        d *= 14;
        d *= 32;
        a += d;
        Z = 0;
    }
    printf("Setup a:%u d:%u\n",a,d);
    START:
        printf("1:: %u %u %u %u %u\n",Z,a,b,c,d);
        c = 1;
        
        
    while(c <= a) {
        b = 1;
        
        /*
        while(b <= a) {
            if(a == c*b) Z += c;
            b += 1;
        }*/
        if(a % c == 0) {
            Z += c;
        }
        b = a + 1;
        c += 1;
        if(c > a) {
            printf("DONE %u %u %u %u %u\n",Z,a,b,c,d);
            return 0;
        }
    }
    
    /*
    L4: 
        printf("2:: %u %u %u %u %u\n",Z,a,b,c,d);
        b = 1;
    L2:
        if(a == c*b) {
            Z += c;
        }
        
    L3:
        b += 1;
        if(!(b > a)) {
            goto L2;
        }
        c += 1;
        if(!(c > a)) {
            goto L4;
        } else {
            printf("DONE %u %u %u %u %u\n",Z,a,b,c,d);
            return 0;
        }
    */
}