import time

INFINITY = float("INF")

class Unit:
    def __init__(self,symbol,x,y,id):
        self.symbol = symbol
        self.hp = 200
        self.strength = 3
        self.x = x
        self.y = y
        self.id = id
        
        
    def getCord(self):
        return (self.x,self.y)
    
    def mov(self,p):
        if(abs(self.x - p[0]) > 1 or abs(self.y -p[1]) > 1):
            raise Error("Illogical move of move than 1 unit")
        #self.debugPrint(self,"moved to",p)
        self.x = p[0]
        self.y = p[1]
        
    def status(self):
        return "Unit: %c  hp(%d) atk(%d) @ %d,%d" % (self.symbol,self.hp,self.strength,self.x,self.y)
        
    def isDead(self):
        return self.hp <= 0
        
    def attack(self,unit):
        if(self.getCord() in adj(unit.getCord())):
            #print("%s hits %s for %d" % (str(self),str(unit),self.strength))
            unit.hp -= self.strength
        else:
            self.debugPrint(str(unit))
            self.debugPrint(str(self))
            raise Error("attack called on 2 non-adjacent units")
        return unit.isDead()
            
    def __str__(self):
        return "%d:%c@(%d,%d)" % (self.id,self.symbol,self.x,self.y)
        
    def __repr__(self):
        return self.__str__()
        
class Map:
    def __init__(self,inputFile,debug=False,elfStr=3):
        self.elfs = 0
        self.goblins = 0
        self.enableDebugPrint = False
        if(debug):
            self.enableDebugPrint = True
        
        grid = open(inputFile).read().strip().split("\n")
        self.debugPrint("Loaded input '%s'" % (inputFile))
        units = []
        
        #Break input into 2d char array
        for i in range(len(grid)):
            grid[i] = list(grid[i])
        
        #Identify units
        unitID = 0
        for y in range(len(grid)):
            for x in range(len(grid[y])):
                c = grid[y][x]
                if(c == "E" or c == "G"):
                    u = Unit(c,x,y,unitID)
                    units.append(u)
                    unitID += 1
                    grid[y][x] = "."
                    if(c == "E"):
                        self.elfs += 1
                        u.strength = elfStr
                    else:
                        self.goblins += 1
                    
        self.grid = grid
        self.dimX = len(grid[0])
        self.dimY = len(grid)
        self.units = units
        self.deceased = []
        self.time = 0
        self.unitDict = {}
        self.updateUnits()
        self.updateUnitDict()
        
        self.elfAttackSpots = None
        self.elfAttackSpotsFresh = False

        self.goblinAttackSpots= None
        self.goblinAttackSpotsFresh = False
        
    def step(self):
        #Sort units by map order
        self.updateUnits()
        #self.updateUnitDict()
        
        #Start iteration over units
        aliveUnits = self.getLivingUnits()
        
        for i in range(len(aliveUnits)):
            unit = aliveUnits[i]
            #Unit died earlier in the turn
            if(unit.isDead()):
                continue
            self.debugPrint("Processing unit:",unit.status())
            
            #Find adjacent attack targets
            targets = self.getAttackTargets(unit)

            #If there are no targets try to move
            if(not targets):
                #Find potential locations
                locations = self.getAttackSpots(unit)
                
                #Find distance to locations
                source = unit.getCord()
                moves = self.getOpenAdj(source)
                self.debugPrint("There are %d valid moves" % (len(moves)) + " " + str(moves))
                bestDistance = INFINITY
                bestMove = None
                bestSpot = None
                for loc in locations:
                    if(loc in self.unitDict):
                        continue
                    distDict = self.exploreFromPoint(loc)
                    for move in moves:
                        if(move in distDict):
                            d = distDict[move]
                            if(d < bestDistance):
                                bestDistance = d
                                bestMove = move
                                bestSpot = loc
                    
                self.debugPrint("\tbest move is " + str(bestMove) + " with distance of " + str(bestDistance) + " to " + str(bestSpot))
                #Move the unit
                if(bestMove is not None):
                    self.moveUnit(unit,bestMove)
                #Try to find targets again
                targets = self.getAttackTargets(unit)
            else:
                self.debugPrint("Not moving, already adjacent to target")

            #Attack if there are targets
            if(targets):
                self.debugPrint("\t targets",str(targets))
                self.debugPrint("\tAttacking!")
                if(unit.attack(targets[0])):
                    if(targets[0].symbol == "E"):
                        self.elfs -= 1
                        self.goblinAttackSpotsFresh = False
                    else:
                        self.goblins -= 1
                        self.elfAttackSpotsFresh = False
                    self.debugPrint("Unit Slain!!:",targets[0].status())
                    del(self.unitDict[targets[0].getCord()])
                    if(self.elfs == 0 or self.goblins == 0):
                        rest = i+1
                        while(rest < len(aliveUnits)):
                            if(not aliveUnits[rest].isDead()):
                                return False
                            rest += 1    
        #Inc time
        self.time += 1
        #print(self.time)
        if((self.elfs == 0 or self.goblins == 0)):
            return False
        if(self.enableDebugPrint):
            self.showMap()
            self.showUnits()
            input("...")
        return True
        
    def displayAnswerPartOne(self):
        #self.showMap()
        #self.showUnits()
        hpSum = 0
        for unit in self.getLivingUnits():
            hpSum += unit.hp
        print("Time:",self.time)
        print("Total hp remaining:",hpSum)
        print("Answer",self.time,"*",hpSum,"=",self.time*hpSum)
        return self.time*hpSum
        
    def resolve(self):
        while(self.step()):
            #input("...")
            pass
        self.debugPrint(self.time)
    
    def dist(source,target):
        v = abs(target[0] - source[0]) + abs(target[1] - source[1])
        #debugPrint("dist from",source,"to",target,"is",v)
        return v
        
    def moveUnit(self,unit,move):
        if(unit.getCord() not in self.unitDict):
            print("ERROR",unit,"NOT IN THE UNIT DICT")
            print(unit.hp)
            for k,v in self.unitDict.items():
                print(k,v)
            raise Error("ASDASD")
            
        #Mutate unit dict to update positions
        del(self.unitDict[unit.getCord()])
        unit.mov(move)
        self.unitDict[unit.getCord()] = unit
        
        #Set tarets to not fresh
        if(unit.symbol == "E"):
            self.goblinAttackSpotsFresh = False
        elif(unit.symbol == "G"):
            self.elfAttackSpotsFresh = False
    
    def exploreFromPoint(self,source):
        known = set()
        known.add(source)
        q = [source]
        dist= {source : 0}
        while q:
            current = q.pop(0)
            for spot in self.getOpenAdj(current):
                if(spot not in known):
                    dist[spot] = dist[current] + 1
                    q.append(spot)
                    known.add(spot)
        return dist
        
    def showUnits(self):
        print("During tick %d" % (self.time))
        print("There are %d elfs and %d goblins." % (self.elfs,self.goblins))
        #self.updateUnits()
        for unit in self.units:
            print(unit.status())
    
    def updateUnits(self):
        Map.sortUnitsMapOrder(self.units)
        return self.units
        
    def sortUnitsMapOrder(units):
        units.sort(key = lambda unit: unit.x)
        units.sort(key = lambda unit: unit.y)
        return units
        
    def sortPointsMapOrder(points):
        points.sort(key = lambda point: point[0])
        points.sort(key = lambda point: point[1])
        return points
        
    def updateUnitDict(self):
        self.unitDict = {}
        for unit in self.getLivingUnits():
            self.unitDict[(unit.x,unit.y)] = unit
        return self.unitDict
        
    def showMap(self):
        self.updateUnitDict()
        print("During tick %d" % (self.time))
        for y in range(len(self.grid)):
            for x in range(len(self.grid[y])):
                p = (x,y)
                if(p in self.unitDict and not self.unitDict[p].isDead()):
                    print(self.unitDict[p].symbol,end="")
                else:
                    print(self.grid[y][x],end="")
            print()
            
    def getAttackTargets(self,unit):
        p = unit.getCord()
        targets = []
        for pA in adj(p):
            if(pA in self.unitDict and unit.symbol != self.unitDict[pA].symbol and not self.unitDict[pA].isDead()):
                targets.append(self.unitDict[pA])
        Map.sortUnitsMapOrder(targets)
        targets.sort(key=lambda u:u.hp)   
        
        return targets
        
    def getLivingUnits(self):
        return [unit for unit in self.units if not unit.isDead()]
        
    def getAttackSpots(self,unit):
        if(unit.symbol == "E"):
            if(self.elfAttackSpotsFresh):
                return self.elfAttackSpots
        elif(unit.symbol == "G"):
            if(self.goblinAttackSpotsFresh):
                return self.goblinAttackSpots
                
        p = unit.getCord()
        spots = set()
        #self.debugPrint("unit dict " + str(len(self.unitDict)))
        for tUnit in self.getLivingUnits():
            if(unit.symbol != tUnit.symbol):
                self.debugPrint(tUnit)
                for spot in self.getOpenAdj(tUnit.getCord()):
                    spots.add(spot)
        spots = list(spots)
        Map.sortPointsMapOrder(spots)
        
        if(unit.symbol == "E"):
            self.elfAttackSpots = spots
            self.elfAttackSpotsFresh = True
        elif(unit.symbol == "G"):
            self.goblinAttackSpots = spots
            self.goblinAttackSpotsFresh = True
        self.debugPrint("Found " + str(len(spots)) + " spots to attack from")
        return spots
        
    def getOpenAdj(self,p):
        openAdj = []
        for newP in adj(p):
            x,y = newP
            if(x > 0 and y > 0 and x < self.dimX and y < self.dimY):
                if((newP not in self.unitDict or self.unitDict[newP].isDead()) and self.grid[y][x] == "."):
                    openAdj.append(newP)
                #elif(self.grid[y][x] != "."):
                #    debugPrint("bad grid val",self.grid[y][x],x,y)
        Map.sortPointsMapOrder(openAdj)
        #debugPrint("openAdj",openAdj)
        return openAdj
        
    def getOpenAdjUnitBlind(self,p):
        openAdj = []
        for newP in adj(p):
            x,y = newP
            if(x > 0 and y > 0 and x < self.dimX and y < self.dimY):
                if(self.grid[y][x] == "."):
                    openAdj.append(newP)
        Map.sortPointsMapOrder(openAdj)
        return openAdj
        
    def debugPrint(self,s,end=None):
        if(self.enableDebugPrint):
            if(end is not None):
                print(s,end=end)
            else:
                print(s)
def adj(p):
    x,y = p
    #debugPrint("Adj",(x,y+1),(x+1,y),(x-1,y),(x,y-1))
    return ((x,y+1),(x+1,y),(x-1,y),(x,y-1))
        
def main():
    
    tests = [("test2.txt",27730),("test3.txt",36334),("test4.txt",39514)]
    for test,answer in tests:
        print("TEST",test)
        m = Map(test)
        #m.showUnits()
        m.showMap()
        print("AFTER RESOLUTION")
        m.resolve()
        simAnswer = m.displayAnswerPartOne()
        if(answer == simAnswer):
            print("SUCCESS")
        else:
            print("FAILED")
            input("...")
    
    t = time.time()
    m = Map("input.txt",debug=False)
    m.showMap()
    m.resolve()
    m.displayAnswerPartOne()
    print("time elapsed",time.time()-t)
    return
    
    #Run a binary search to find lowest working elf str
    upper = 20
    lower = 3
    results = {}
    
    while(lower+1 != upper):
        current = ((upper - lower)//2) + lower
        m = Map("input.txt",debug=False,elfStr=current)
        print("lower",lower,"current",current,"upper",upper)
        startingElfs = m.elfs
        
        while(True):
            result = m.step()
            if(m.elfs != startingElfs):
                lower = current
                #print("New lower bound")
                break
            elif(not result):
                upper = current
                results[upper] = m.displayAnswerPartOne()
                #print("New upper bound")
                break
    print("With elf str",upper,"elfs barely win without a loss of life with score:",results[upper])
    
    
if __name__ == "__main__":
    main()