n = 9445


def powerLevel(p,input):
    rackid = p[0]+10
    power = rackid * p[1]
    power += input
    power *= rackid
    power = (power % 1000) // 100
    power -= 5
    return power

def slidingWindowIter(grid,size,xLimit,yLimit):
    x = 0
    y = 0
    sumNow = 0
    #Get starting sum
    for a in range(size):
        for b in range(size):
            sumNow += windowSafeAccess(grid,x+a,y+b)
    
    while(x >= xLimit-size and y >= yLimit-size):
        #Move right
        while(x < xLimit-size):
            for b in range(size):
                sumNow -= windowSafeAccess(grid,x,y+b)
                sumNow += windowSafeAccess(grid,x+size+1,y+b)
                #sumNow -= grid[(x,y+b)]
                #sumNow += grid[(x+size+1,y+b)]
            windowUpdateMax(sumNow,x,y,size)
            x += 1
        #Step down
        y += 1
        for a in range(size):
            sumNow -= windowSafeAccess(grid,x+a,y)
            sumNow += windowSafeAccess(grid,x+a,y+size+1)
            #sumNow -= grid[(x+a,y)]
            #sumNow += grid[(x+a,y+size+1)]
        windowUpdateMax(sumNow,x,y,size)
        #Move left
        while(x > 0):
            for b in range(size):
                sumNow -= windowSafeAccess(grid,x+size,y+b)
                sumNow += windowSafeAccess(grid,x-1,y+b)
                #sumNow -= grid[(x,y+b)]
                #sumNow += grid[(x+size+1,y+b)]
            windowUpdateMax(sumNow,x,y,size)
            x -= 1
        
        #Step down
def windowSafeAccess(grid,x,y):
    v = (x,y)
    if(v in grid):
        return grid[v]
    else:
        print("OOB",v)
        input()
    return 0
maxCords = (-1,-1)
maxSum = 0
maxSize = 0
def windowUpdateMax(sumNow,x,y,size):
    if(sumNow > maxSum):
        maxCords = (x,y)
        maxSum = sumNow
        maxSize = size
        
def neighborSum(p,grid,size):
    nSum = 0
    
    for x in range(size):
        for y in range(size):
            if((p[0]+x,p[1]+y) in grid):
                nSum += grid.get((p[0]+x,p[1]+y),0)
            else:
                pass
    return nSum
grid = {}

for x in range(1,301):
    for y in range(1,301):
        grid[(x,y)] = powerLevel((x,y),n)

#gridSum = {}


maxP = (-1,-1)
maxV = 0
maxSize = 0
for size in range(1,301):
    print("checking size",size)
    for x in range(1,301-size):
        for y in range(1,301-size):
            newV = neighborSum((x,y),grid,size)
            if(newV > maxV):
                maxV = newV
                maxP = (x,y)
                maxSize = size
                print("New best",newV,(x,y),"size",size)
print(maxV)
print(maxP)
print(size)