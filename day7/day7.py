lines = open("input","r").read().strip().split("\n")
g = {}
req = {}
for l in lines:
	a,b = l.split()
	g[a] = g.get(a,[]) + [b]
	req[b] = req.get(b,[]) + [a]

for k,v in g.items():
	print(k,v)

print("REQ")
for k,v in req.items():
	print(k,v)

frontier = []
for c in list("ABCDEFGHIJKLMNOPQRSTUVWXYZ"):
	if c not in req:
		frontier.append(c)
frontier.sort()
start = frontier.copy()

done = set()
order = ""
while frontier:
	current = frontier.pop(0)
	done.add(current)
	order += current
	n = g.get(current,[])
	for x in n:
		if(x in done or x in frontier):
			continue
		ready = True
		if(x in req):
			for y in req[x]:
				if(y not in done):
					ready = False
					break
		if(ready):
			frontier.append(x)
	frontier.sort()

print(order)

workers = [None] * 5
workersTime = [-1]*5
index = 0
time = 0
nDone = set()
print("part2 start",start)
while len(nDone) < 26:
	print("#",time,end="::  ")
	for i in range(len(workers)):
		p = workers[i]
		if(p is None):
			p = "."
		print("%1s(%3d) " % (workers[i],workersTime[i]),end="")
	print()
				
	#Check for jobs that are done
	for i in range(len(workers)):
		if(workersTime[i] <= 0 and workers[i] is not None):
			print(time,workers[i],"done")
			nDone.add(workers[i])
			c = workers[i]
			workers[i] = None
			#Add new jobs
			if(c in g):
				for x in g[c]:
					if(x in nDone or x in start):
						continue
					ready = True
					if(x in req):
						for y in req[x]:
							if(y not in nDone):
								ready = False
								break
					if(ready):
						start.append(x)	

	#Assign jobs
	start.sort()
	while start:
		for i in range(len(workers)):
			if(workers[i] is None):
				#Assign job
				workers[i] = start.pop(0)
				workersTime[i] = ord(workers[i]) - ord("A") + 61
				print(time,workers[i],"started")
				if(start):
					continue
				else:
					break
		break

	#Decrement workerTime
	for i in range(len(workersTime)):
		if(workersTime[i] > 0):
			workersTime[i] -= 1
	time += 1	
	
print(time)
