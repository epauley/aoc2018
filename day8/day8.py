nums = [int(x) for x in open("input","r").read().strip().split()]
nodes = []
def readChild(index,nums):
    nChild = nums[index]
    index += 1
    nMeta = nums[index]
    index += 1
    sum = 0
    for c in range(nChild):
        index,pSum = readChild(index,nums)
        sum += pSum
    for x in range(nMeta):
        sum += nums[index]
        index += 1
    return (index,sum)

def readChild2(index,nums):
    nChild = nums[index]
    index += 1
    nMeta = nums[index]
    index += 1
    sum = 0
    childValues = []
    if(nChild == 0):
        for x in range(nMeta):
            sum += nums[index]
            index += 1
        return index,sum
        
    for c in range(nChild):
        index,pSum = readChild2(index,nums)
        childValues.append(pSum)
        #sum += pSum
   
    for x in range(nMeta):
        k = nums[index]-1
        if(k < len(childValues)):
            sum += childValues[k]
        index += 1
    return (index,sum)    

print(readChild2(0,nums))