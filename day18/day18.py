"""An open acre will become filled with trees if three or more adjacent acres contained trees. Otherwise, nothing happens.

An acre filled with trees will become a lumberyard if three or more adjacent acres were lumberyards. Otherwise, nothing happens.

An acre containing a lumberyard will remain a lumberyard if it was adjacent to at least one other lumberyard and at least one acre containing trees. Otherwise, it becomes open.

These changes happen across all acres simultaneously, each of them using the state of all acres at the beginning of the minute and changing to their new form by the end of that same minute. Changes that happen during the minute don't affect each other."""

inp = open("input.txt").read().strip().split("\n")
grid = []

for l in inp:
    grid.append(list(l))

maxY = len(grid)
maxX = len(grid[0])
def adj(x,y):
    a = [   (x-1,y+1),  (x,y+1),    (x+1,y+1),
            (x-1,y),                (x+1,y),
            (x-1,y-1),  (x,y-1),    (x+1,y-1)]
    i = 0
    while i < len(a):
        tx,ty = a[i]
        if(not ((0 <= tx < maxX) and (0 <= ty < maxY))):
            a.pop(i)
        else:
            i+=1
    
    return a
def newC(x,y,grid):
    lumber = 0 
    ground = 0
    forest = 0
    for p in adj(x,y):
        px,py = p
        c = grid[py][px]
        if(c == "."):
            ground += 1
        elif(c == "#"):
            lumber += 1
        elif(c == "|"):
            forest += 1
            
    here = grid[y][x]
    if(here == "."):
        if(forest >= 3):
            return "|"
        else:
            return "."
    elif(here == "|"):
        if(lumber >= 3):
            return "#"
        else:
            return "|"
    elif(here == "#"):
        if(lumber >= 1 and forest >= 1):
            return "#"
        else:
            return "."
    return None
            
    
def show(grid):
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            print(grid[y][x],end="")
        print()
        
def step(grid,newGrid):
    h = 0
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            c = newC(x,y,grid)
            newGrid[y].append(c)
            if(c == "."):
                h = (h * 3)
            elif(c == "|"):
                h = (h * 3) + 1
            elif(c == "#"):
                h = (h * 3) + 2
            
    return newGrid,h

def resourceValue(grid):
    lumber = 0
    forest = 0
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            if(grid[y][x] == "#"):
                lumber += 1
            elif(grid[y][x] == "|"):
                forest += 1
    print("lumber",lumber,"forest",forest,"result",lumber*forest)

t = 0
seen = {}
for x in range(10):
    t += 1
    grid,hash = step(grid)
    
    #input("...")
print("Part 1")
resourceValue(grid)

while True:
    t += 1
    grid,hash = step(grid)
    #show(grid)
    print(t)
    if hash in seen:
        print("Repeated step found")
        diff = t - seen[hash]
        target = 1000000000
        remaining = target - t
        cycles = remaining // diff
        
        t = t + (cycles * diff)
        while(t < target):
            grid = step(grid)
            t += 1
        print("Part 2")
        resourceValue(grid)
        break
    else:
        seen[hash] = t
    #input("...")

    