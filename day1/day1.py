import sys
import math
"""
#PART ONE
sum = 0
while True:
    sum += int(input())
    print(sum)
"""
    
"""
#PART TWO
nums = [int(x) for x in open("input1.txt","r").read().strip().split()]
i = 0
freq = 0
known = set([freq])
while True:
    freq += nums[i]
    if(freq not in known):
        known.add(freq)
    else:
        print(freq)
        break
       
    
    i = (i+1) % len(nums)
"""

#PART TWO, MATHY SOLUTION
nums = [int(x) for x in open("input1.txt","r").read().strip().split()]
i = 0
freq = 0
known = set([freq])

#Run through the list once, creating a prefix sum list
prefix = []
for n in nums:
	freq += n
	if(freq in known):
		print(freq)
		sys.exit(0)
	else:
		known.add(freq)
	prefix.append(freq)


#Calculate the delta of applying the whole list, or the last element of the prefix sum
delta = prefix[-1]
if(delta == 0):
	print("No collision will happen, delta is 0")
	sys.exit(0)

#If delta is negative, negate delta, and the prefix sums
negateAnswer = False
#Create a new list of the prefix sums modulo the value of the whole list
if(delta < 0):
	negateAnswer = True
	delta *= -1
	for i in range(len(prefix)):
		prefix[i] *= -1
	
prefixModulo = [x % delta for x in prefix]

#If two numbers modulo the delta are equal, they will collide eventually
#Create a list of eventual collisions
matches = {}
check = set()
for i in range(len(prefixModulo)):
	n = prefixModulo[i]
	if(n in matches):
		#Flag this entry to be checked
		check.add(n)
	matches[n] = matches.get(n,[]) + [i]

#Identify which collision will happen first
#First by time until collision, and then by index
#Find k in k*d + x = y
#	d is the delta
#	x is the lower value
#	y is the higher value
#	k is some positive integer
#k = (y - x)/d

collisionTime = float("Inf")
collisionIndex = -1
collisionValue = -1

for c in check:
	#Store collisions with indexes into values
	values = [(prefix[x],x) for x in matches[c]]

	#Only check the 2 closest values, with the lowest index
	values.sort()
	closestIndex = -1
	closestDist = float("Inf")
	xIndex = float("Inf")
	for i in range(len(values)-1):
		dist = values[i][0] - values[i+1][0]
		if(dist < 0):
			dist *= -1
		if(dist <= closestDist and values[i][1] < xIndex):
			closestIndex = i
			closestDist = dist	
			xIndex = values[i][1]
	
	#Calculate k = (y-x)/d
	pair = (values[closestIndex][0],values[closestIndex+1][0])	
	(x,y) = min(pair),max(pair)

	k = (y-x)/delta
	#print(pair)
	#print(k,"= (",y,"-",x,")/",delta)
	#print(k*delta + x)
	if(k < collisionTime or (k==collisionTime and xIndex<collisionIndex)):
		collisionTime = int(k)
		collisionIndex = xIndex
		collisionValue = y

if(negateAnswer):
	collisionValue *= -1
print("Soonest collision at index",collisionIndex,"and loop",collisionTime)
print(prefix[collisionIndex])
print(collisionValue)
