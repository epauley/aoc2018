lines = open("input","r").read().strip().split("\n")
#123 @ 3,2: 5x4
result = ""
d = {}
ident = {}
for l in lines:
	iden,_,cord,dim =  l.split()
	cord = cord[:-1]
	x,y = [int(x) for x in cord.split(",")]
	w,h = [int(q) for q in dim.split("x")]
	
	ident[(x,y)] = [iden,x,y,w,h]
	
	for a in range(w):
		for b in range(h):
			xcord = x+a
			ycord = y+b
			p = (xcord,ycord)
			d[p] = d.get(p,0) + 1

total = 0
for k,v in d.items():
	if(v >= 2):
		total += 1
	if(v == 1):
		if(k in ident):
			iden,x,y,w,h = ident[k]
			f = True
			for a in range(w):
				for b in range(h):
					if(d[(a+x,b+y)] != 1):
						f = False
						break
			if(f):
				print(ident[k])

print(total)
