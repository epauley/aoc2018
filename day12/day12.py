import random
import os
import sys
RULELEN = 13

def binRules(rules):
    binRules = [None]*len(rules)
    l = 0
    for k,v in rules.items():
        bVal = 0
        for c in k:
            if(c == "#"):
                bVal = (bVal << 1) + 1
            else:
                bVal = bVal << 1
        binRules[bVal] = v
        #print(k,bVal,v)
        l += 1
    print("Generated a binrules with",l,"items")
    print(binRules)
    input()
    return binRules

def parseInput(filename,setRULELEN=True):
    inp = open(filename,"r").read().strip().split("\n")
    state = inp[0].split(": ")[1]
    inp = inp[2:]
    inp.sort()
    rules = {}
    print("Loading rules from",filename,"...")
    for l in inp:
        pattern,outcome = l.split(" => ")
        rules[pattern] = outcome
        print(pattern,":",outcome)
    global RULELEN
    if(setRULELEN):
        RULELEN = len(inp[0].split(" => ")[0])
        print("Setting RULELEN to",RULELEN)
    return state,rules

def generation(state,leftMost,binrules,gen):
    leftMost -= RULELEN
    state = ("."*RULELEN) + state + ("."*RULELEN)
    #state = state + ("."*RULELEN)
    newState = ["*"]*(len(state))
    #print(newState)
    
    #Sliding window preload
    window = 0
    windowMask = (2**RULELEN)-1
    for i in range(len(state)-(RULELEN)):
        print("{0:b}".format(window),state[i])
        window = ((window << 1) + (state[i]=="#")) & windowMask
        print("{0:b}".format(window))
        print()
        #p = state[i:i+RULELEN]
        #print(window)
        newState[i+(RULELEN//2)] = binrules[window]
    while(len(newState) > 0 and newState[0] != "#"):
        newState.pop(0)
        leftMost += 1
    
    while(len(newState) > 0 and newState[-1] != "#"):
        newState.pop()
    return "".join(newState),leftMost,gen+1

def potSum(state,leftMost):
    pSum = 0
    for i in range(len(state)):
        if(state[i] == "#"):
            pSum += i + leftMost
    return pSum
    

def partOne(state,rules):
    leftMost = 0
    gen = 0
    binrules = binRules(rules)
    for i in range(20):
        if(state == ""):
           break
        state,leftMost,gen = generation(state,leftMost,binrules,gen)
        
    print("Part 1:",potSum(state,leftMost))
    return state,leftMost,gen

    

def partTwo(state,rules,leftMost,gen,verbose=False,neverStop=False):
    startLen = len(state)
    stateDict = {}
    postpone = 1000
    binrules = binRules(rules)
    print(binrules)
    while(state not in stateDict):
        stateDict[state] = (leftMost,gen)
        state,leftMost,gen = generation(state,leftMost,binrules,gen)
        
        if(state == ""):
            print("Empty state, no plants ever again")
            return 0
        if(not neverStop):
            if(gen >= postpone):
                print("no cycle after",postpone,"cycles")
                if(len(state) > 10*startLen):
                    print("the length of the state has blown up and will likely continue, quitting")
                    return 0
                else:
                    print("the length of state hasn't blown up yet, postponing 1000 cycles")
                    postpone += 1000
        if(verbose):
            #print(state)
            print("Gen:",gen,"leftMost:",leftMost,"statelen:",len(state))
    
    oldLeftMost,oldGen = stateDict[state]    
    cycleLength = gen-oldGen     
    migration = leftMost-oldLeftMost
    print("Stablized at Generation",gen)
    print("Cycle is of length",cycleLength,"after which a migration of",migration,"occurs")
    fiddyBill = 50000000000
    if(cycleLength == 1):
        leftMost += (migration) * (fiddyBill - gen)
    else:
        
        cycles = (fiddyBill - gen) // (gen-oldGen)
        leftMost += cycles * (migration)
        gen += cycles*cycleLength
        while(gen != fiddyBill):
            state,leftMost,gen = generation(state,leftMost,binrules,gen)

    print("Part 2:",potSum(state,leftMost))
    return cycleLength

def ruleGenerator(randomNumber):
    print("Seeding with",randomNumber)
    random.seed(randomNumber)
    rules = {}
    for i in range(2**RULELEN):
        fString = "{0:b}".format(i)
        fString = ("0" * (RULELEN-len(fString))) + fString
        fString = fString.replace("0",".")
        fString = fString.replace("1","#")
        value = (".","#")[random.randint(0,1)]
        if(fString.count(".") >= RULELEN-1):
            rules[fString] = "."
        else:
            rules[fString] = value
        #print(fString,value)
    #print("-"*20)
    return rules
    

def main():
    if(len(sys.argv) > 1):
        if(sys.argv[1] == "seed"):
            seed = int(sys.argv[2])
            state,rules = parseInput("input",setRULELEN=False)
            rules = ruleGenerator(seed)
            state,leftMost,gen = partOne(state,rules)
            partTwo(state,rules,leftMost,gen,verbose=True,neverStop=True)
            return 0
        else:
            state,rules = parseInput(sys.argv[1])
            state,leftMost,gen = partOne(state,rules)
            partTwo(state,rules,leftMost,gen,verbose=True)
            return 0
    
    input("Start generation?")

    state,rules = parseInput("input",setRULELEN=False)
    global RULELEN
    RULELEN = 9
    foldername = "len" + str(RULELEN)
    if(not os.path.exists(foldername)):
        os.mkdir(foldername)
        print("made dir",foldername)
    startState = state
    CYCLE_SEARCH = 2
    while(True):
        seed = random.randint(0,2**64)
        state = startState
        rules = ruleGenerator(seed)
        state,leftMost,gen = partOne(state,rules)
        r = partTwo(state,rules,leftMost,gen)
        if(r > CYCLE_SEARCH):
            with open(foldername+"/newInput" + str(r),"w") as f:
                f.write("initial state: ")
                f.write(startState)
                f.write("\n\n")
                
                
                for k,v in rules.items():
                    f.write(k)
                    f.write(" => ")
                    f.write(v)
                    f.write("\n")
            print("Wrote rules out to newInput")
            CYCLE_SEARCH = r
            print("Setting CYCLE_SEARCH to",r)
    
if __name__ == "__main__":
    main()

