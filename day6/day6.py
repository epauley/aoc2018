import math
import random
from PIL import Image
inp = open("input","r").read().strip().split("\n")


cords = []
for l in inp:
	x,y = [int(x) for x in l.split(",")]
	cords.append((x,y))

cords.sort()
for k in cords:
	print(k)


cordIndex = {}

xMIN = cords[0][0] 
xMAX = cords[-1][0] 
yMIN = min([y for _,y in cords])
yMAX = max([y for _,y in cords])
print("Dimensions are ",xMAX-xMIN,yMAX-yMIN)

for i in range(len(cords)):
	cordIndex[i] = cords[i]
grid = []
gridSum = []
for x in range(xMIN,xMAX):
	grid.append([])
	gridSum.append([])
	for y in range(yMIN,yMAX):
		grid[-1].append([-1,99999])
		gridSum[-1].append(0)

regionSum = 0
for x in range(xMIN,xMAX):
	for y in range(yMIN,yMAX):
		bestIndex,bestDist = grid[x-xMIN][y-yMIN]
		for a,b in cords:
			dist = abs(x-a) + abs(y-b)
			gridSum[x-xMIN][y-yMIN] += dist
			
			thisIndex = cords.index((a,b))
			dist = abs(x-a) + abs(y-b)
			if(dist == bestDist):
				bestIndex.append(thisIndex)
			elif(dist < bestDist):
				bestIndex = [thisIndex]
				bestDist = dist
				
			grid[x-xMIN][y-yMIN] = (bestIndex,bestDist)
		if(gridSum[x-xMIN][y-yMIN] < 10000):
			regionSum += 1


print("PART 2",regionSum)
		
img = Image.new("RGB",(xMAX-xMIN,yMAX-yMIN),color="black")

	
print("BUCKET FILLLLLL")

pix = img.load()
for x in range(xMIN,xMAX):
	for y in range(yMIN,yMAX):
		if(gridSum[x-xMIN][y-yMIN] < 10000):
			v = int(255 * (gridSum[x-xMIN][y-yMIN]/5000))
			pix[x-xMIN,y-yMIN] = (v//2,0,v)
img.save("region.png","PNG")

		

cordsCount = {}
disQualify = set()
for x in range(xMIN,xMAX):
	bestIndex,bestDist = grid[x-xMIN][0]
	if(len(bestIndex) == 1):
		disQualify.add(bestIndex[0])

	bestIndex,bestDist = grid[x-xMIN][yMAX-1-yMIN]
	if(len(bestIndex) == 1):
		disQualify.add(bestIndex[0])

for y in range(yMIN,yMAX):
	bestIndex,bestDist = grid[0][y-yMIN]
	if(len(bestIndex) == 1):
		disQualify.add(bestIndex[0])

	bestIndex,bestDist = grid[xMAX-1-xMIN][y-yMIN]
	if(len(bestIndex) == 1):
		disQualify.add(bestIndex[0])

for k in disQualify:
	print(k,"is disqualified")

for x in range(xMIN,xMAX):
	for y in range(yMIN,yMAX):
		bestIndex,bestDist = grid[x-xMIN][y-yMIN]
		if(len(bestIndex) == 1):	
			cordsCount[bestIndex[0]] = cordsCount.get(bestIndex[0],0) + 1

bests = []
for k,v in cordsCount.items():
	if(k not in disQualify):
		bests.append((v,k))
		print(v,k)
	
print("---->",max(bests))

colors = []
for i in range(len(cords)):
	colors.append((random.randint(0,256),random.randint(0,256),random.randint(0,256)))
	
img = Image.new("RGB",(xMAX-xMIN,yMAX-yMIN),color="black")
pix = img.load()
for x in range(xMIN,xMAX):
	for y in range(yMIN,yMAX):
		bestIndex, bestDist = grid[x-xMIN][y-yMIN]
		if(len(bestIndex) == 1):
			colors[bestIndex[0]]
			pix[x-xMIN,y-yMIN] = colors[bestIndex[0]]
img.save("part1.png","PNG")
