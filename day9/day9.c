#include <stdio.h>
#include <stdlib.h>

typedef struct node{
    struct node *next;
    struct node *prev;
    int id;
} tNode;

int marbleLL(int players, unsigned long maxValue) {
    tNode* current;
    tNode* new;
    tNode* a;
    tNode* b;
    current = malloc(sizeof(tNode));
    current->id = 0;
    current->next = malloc(sizeof(tNode));
    current->prev = current->next;
    current->next->next = current;
    current->next->prev = current;
    current->id = 1;
    
    current = current->next;
    int currentPlayer = 1;
    unsigned long *playerScore = calloc(players,sizeof(unsigned long));
    unsigned long placed = 2;
    unsigned long maxScore = 0;
    unsigned long maxScorePlayer = 0;
    while(1) {
        if(placed % 23 == 0) {
            current = current->prev->prev->prev->prev->prev->prev->prev;
            playerScore[currentPlayer] += current->id + placed;
            if(playerScore[currentPlayer] > maxScore) {
                maxScore = playerScore[currentPlayer];
                maxScorePlayer = currentPlayer;
                //printf("new max score %d for player %d, adding id %d and marble %d\n",maxScore,maxScorePlayer,current->id,placed);
            }
            a = current->prev;
            b = current->next;
            a->next = b;
            b->prev = a;
            free(current);
            current = b;
        } else {
            new = malloc(sizeof(tNode));
            current = current->next;
            new->prev = current;
            new->next = current->next;
            new->next->prev = new;
            new->id = placed;
            current->next = new;
            current = new;
        }
        if(placed == maxValue) break;
        placed += 1;
        currentPlayer = (currentPlayer + 1) % players;
    }
    return maxScore;
}

int main(int argc, char** argv) {
    //int p = 473; long v = 70904*100;
    //int p = 9; long v = 25;
    //int p = 10; long v = 1618;
    if(argc < 3) {
        printf("Usage: Pass in player count and max marble as arguements\n");
        return 1;
    }
    unsigned p = atoi(argv[1]);
    unsigned v = atoi(argv[2]);
    printf("%u\n",marbleLL(p,v));
    return 0;
}