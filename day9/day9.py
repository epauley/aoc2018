#p = 473
#v = 70904
#p = 9
#v = 25

tests = [   [10,1618,8317],
            [9,25,32],
            [13,7999,146373]]
#p,v,result = [10,1618,8317] #8317

class M:
    def __init__(self,id):
        self.next = None
        self.prev = None
        self.id = id
        
def marbleLL(p,v):
    current = M(0)
    one = M(1)
    current.next = one
    current.prev = one
    one.next = current
    one.prev = current
    current = one
    
    players = 1
    pScore = [0] * p
    placed = 2
    while True:
        if(placed % 23 == 0):
            for k in range(7):
                current = current.prev
            pScore[players] += current.id + placed
            
            
            a = current.prev
            b = current.next
            a.next = b
            b.prev = a
            del(current)
            current = b
        else:
            n = M(placed)
            current = current.next
            n.prev = current
            n.next = current.next
            n.next.prev = n
            current.next = n
            current = n
            
        if(placed == v):
            break
        placed += 1
        players = (players + 1) % p
    return max(pScore)  
    
    

def marbleFun(p,v):
    marbles = [0]
    current = 0
    player = 0
    playerScore = [0] * p
    placed = 1
    while True:
        if(placed % 23 == 0):
            playerScore[player] += placed
            eightBack = current - 8
            if(eightBack < 0):
                eightBack = eightBack + len(marbles)
            #nextItem = marbles[(eightBack + 1) % len(marbles)]
            if(eightBack+2 == len(marbles)):
                current = 0
            else:
                current = eightBack+1
            playerScore[player] += marbles.pop(eightBack)
            #current = marbles.index(nextItem)+1
            #current = 
        
        else:
            aIndex = (current + 1) % (len(marbles))
            if(aIndex == 0):
                aIndex = len(marbles)
            marbles.insert(aIndex,placed)
            current = (aIndex+1) % len(marbles)
            #current = marbles.index(placed) + 1
        #print(marbles)
        #print(current,"=",marbles[current])
        
        if(placed == v):
            break
        #input()
        
        placed += 1
        player += 1
        player = player % p
    return max(playerScore)
print(marbleLL(473,70904*100))
for t in tests:
    p,v,res = t
    r = marbleLL(p,v)
    print("TEST",p,v,res)
    print("\t",r,res,r==res)
    print("\n")
