ops = "addr addi mulr muli banr bani borr bori setr seti gtir gtri gtrr eqri eqir eqrr".split()

def addr(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a]+regs[b]
    return tuple(newRegs)

def addi(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a]+b
    return tuple(newRegs)

def mulr(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a]*regs[b]
    return tuple(newRegs)

def muli(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a]*b
    return tuple(newRegs)
    
def banr(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a] & regs[b]
    return tuple(newRegs)
    
def bani(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a] & b
    return tuple(newRegs)

def borr(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a] | regs[b]
    return tuple(newRegs)
    
def bori(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a] | b
    return tuple(newRegs)
    
def setr(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = regs[a]
    return tuple(newRegs)
    
def seti(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = a
    return tuple(newRegs)
    
def gtir(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = int(a > regs[b])
    return tuple(newRegs)
    
def gtri(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = int(regs[a] > b)
    return tuple(newRegs)
    
def gtrr(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = int(regs[a] > regs[b])
    return tuple(newRegs)
    
def eqir(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = int(a == regs[b])
    return tuple(newRegs)
    
def eqri(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = int(regs[a] == b)
    return tuple(newRegs)
    
def eqrr(regs,opcode):
    id,a,b,c = opcode
    newRegs = list(regs)
    newRegs[c] = int(regs[a] == regs[b])
    return tuple(newRegs)
    
def matchOps(funcs,opcode,regBefore,regAfter):
    matches = []
    for f,name in funcs:
        if(f(regBefore,opcode) == regAfter):
            matches.append(name)
    return set(matches)

funcs = [   (addr,"addr"),
            (addi,"addi"),
            (mulr,"mulr"),
            (muli,"muli"),
            (banr,"banr"),
            (bani,"bani"),
            (borr,"borr"),
            (bori,"bori"),
            (setr,"setr"),
            (seti,"seti"),
            (gtri,"gtri"),
            (gtrr,"gtrr"),
            (gtir,"gtir"),
            (eqrr,"eqrr"),
            (eqri,"eqri"),
            (eqir,"eqir")
        ]
        
funcDict = {}
for v,k in funcs:
    funcDict[k] = v
inp = open("input.txt","r").read().strip().split("\n")
i = 0

opCandidates = {}
for k in range(len(funcs)):
    opCandidates[k] = set(ops)
threeOrMore = 0
while True:
    if(inp[i].startswith("Before:")):
        regBefore = tuple(int(x) for x in inp[i].split("Before: ")[1][1:-1].split(","))
        
        op = tuple(int(x) for x in inp[i+1].split())
        regAfter = tuple(int(x) for x in inp[i+2].split("After:  ")[1][1:-1].split(","))
        print(regBefore,op,regAfter)
        m = matchOps(funcs,op,regBefore,regAfter)
        if(len(m) >= 3):
            threeOrMore +=1
        print(m)
        opCandidates[op[0]].intersection_update(m)
        i += 4
    else:
        break
i += 2
        
known = {}

while True:
    exit = True
    #Select singleton
    sk,sv = None,None
    for k,v in opCandidates.items():
        if(len(v) == 1):
            known[k] = list(v)[0]
            sk,sv = k,v
            exit = False
            break
            
    if(exit):
        break
    
    #Remove singleton
    del(opCandidates[sk])
    
    #Remove from all other sets
    for k,v in opCandidates.items():
        opCandidates[k].difference_update(sv)
print("KNOWN")
for k,v in known.items():
    print(k,v)
    
regs = (0,0,0,0)
print(regs)
while i < len(inp):
    op = tuple(int(x) for x in inp[i].split())
    regs = funcDict[known[op[0]]](regs,op)
    print(regs)
    i += 1
print("Three or more:",threeOrMore)