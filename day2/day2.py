import sys
lines = open("input1.txt","r").read().strip().split()

def twoOfAny(s):
	m = {}
	for c in s:
		m[c] = m.get(c,0) + 1
	two = False
	three = False
	for k,v in m.items():
		if(v == 2):
			two = True
		if(v == 3):
			three = True
	return two,three

twoC = 0
threeC = 0
for l in lines:
	two,three = twoOfAny(l)
	twoC += two
	threeC+= three
print(twoC*threeC)
print(twoC)
print(threeC)

def dist(a,b):
	diff = 0
	for i in range(len(a)):
		if(a[i] != b[i]):
			diff += 1
		if(diff == 2):
			return False
	return True
		

def part2dictSpam(L):
	D = {}
	for item in L:
		for i in range(len(item)):
			s = item[:i] + item[i+1:]
			if(s in D):
				o = D[s]
				if(o[0] != s and o[1] == i):
					print(item)
					print(o[0])
					sys.exit(0)
			else:
				D[s] = [item,i]
part2dictSpam(lines)
