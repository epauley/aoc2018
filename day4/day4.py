lines = open("input","r").read().strip().split("\n")

lines.sort()

days = {}
guards = {}
guardDay = {}
for l in lines:
	timestamp,message = l[1:].split("]",1)
	date,time = timestamp.split(" ")
	hour,minute = time.split(":")
	year,month,day = date.split("-")
	dateTuple = (year,month,day)
	if("Guard" in message):
		gId,action = message.split(" ",3)[2:]
		currentGuard = gId
		currentDay = (year,month,day)
	elif("wakes up" in message):
		currentDay = (year,month,day)
		guardDay[gId] = guardDay.get(gId,[]) + [dateTuple]
		days[dateTuple] = days.get(dateTuple,[]) + [[int(minute),"wake"]]
		
	elif("falls asleep" in message):	
		currentDay = (year,month,day)
		days[dateTuple] = days.get(dateTuple,[]) + [[int(minute),"sleep"]]
		guardDay[gId] = guardDay.get(gId,[]) + [dateTuple]
	
guardSleepDict = {}
for g, ds in guardDay.items():
	ds = set(ds)
	print(g,"worked",len(ds),"days")
	for year,month,day in ds:
		events = days[(year,month,day)]
		events.sort()
		#print(events)
		eventIndex = 0
		guardSleep = []
		awake = True
		for i in range(60):
			if(eventIndex < len(events)):
				if(i == events[eventIndex][0]):
					if(events[eventIndex][1] == "wake"):
						awake = True
						eventIndex += 1
					elif(events[eventIndex][1] == "sleep"):
						awake = False 
						eventIndex += 1
			if(awake):
				guardSleep.append(1)
			else:
				guardSleep.append(0)
		#print(guardSleep)
		guardSleepDict[g] = guardSleepDict.get(g,[]) + [guardSleep]

asleepList = []
for gId,sleep in guardSleepDict.items():
	sleepSum = 0
	for d in sleep:
		sleepSum += 60-sum(d)
	asleepList.append([sleepSum,gId])

mostAsleep = -1 
mostAsleepId = -1 
for sAvg,gId in asleepList:
	if(sAvg > mostAsleep):
		mostAsleep = sAvg
		mostAsleepId = gId
	print(sAvg,gId)

print(mostAsleepId,"was asleep the most")
idVal = int(mostAsleepId[1:])

sleep = guardSleepDict[mostAsleepId]
mins = [0] * 60

asleepMinute = mins.index(min(mins))
print(asleepMinute,idVal)
print(asleepMinute*idVal)

#part 2
mostAsleepVal = -1 
mostAsleepGuard = -1
mostAsleepMin = -1
for gId,sleep in guardSleepDict.items():	
	mins = [0] * 60
	for s in sleep:
		for i in range(60):
			mins[i] += s[i]
	asleepValue = len(sleep) - min(mins)
	asleepMinute = mins.index(min(mins))
	if(asleepValue > mostAsleepVal):
		mostAsleepVal = asleepValue
		mostAsleepMin = asleepMinute
		mostAsleepGuard = gId
		print("new best",mins)
		print("val",asleepValue,"min",asleepMinute)
		for i in range(60):
			print(i,":",mins[i])
mostAsleepGuard = int(mostAsleepGuard[1:])
print("part2")
print(mostAsleepVal,mostAsleepMin,mostAsleepGuard)
print(mostAsleepGuard*mostAsleepMin)
