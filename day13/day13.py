def findFirstCollision(arr):
	carts = []
	cartTurns = {}
	cartCount = 0
	tick = 0
	for y in range(len(arr)):
		for x in range(len(arr[y])):
			c = arr[y][x]
			if(c in "><^v"):
				carts.append([(x,y),cartCount,c,0,False])
				cartCount += 1
				cartTurns[cartCount] = []
				if(c in "><"):
					arr[y][x] = "-"
				elif(c in "^v"):
					arr[y][x] = "|"
	while True:
		#Calculate next tick
		carts.sort(key=lambda x: x[0][0])
		carts.sort(key=lambda x: x[0][1])
		
		for i in range(len(carts)):
			coords,cartID,cartSymbol,turn,crashed = carts[i]
			if(crashed):
				continue
			cart = carts[i]
			#print(cart)
			x,y = coords
			dx,dy = 0,0
			if(cartSymbol == "^"):
				dy = -1
			elif(cartSymbol == "v"):
				dy = 1
			elif(cartSymbol == ">"):
				dx = 1
			elif(cartSymbol == "<"):
				dx = -1
			else:
				raise ERROR
			newX,newY = x+dx,y+dy
			#print(newX,newY)
			nextChar = arr[newY][newX]
			#print("Next char",nextChar)
			
			#If it is a turn, turn
			if(nextChar == "/"):
				if(cartSymbol == "^"):
					cart[2] = ">"
				elif(cartSymbol == "v"):
					cart[2] = "<"
				elif(cartSymbol == "<"):
					cart[2] = "v"
				elif(cartSymbol == ">"):
					cart[2] = "^"
				else:
					raise ERROR
				#print("TURNED",cart[2])
			elif(nextChar == "\\"):
				if(cartSymbol == "^"):
					cart[2] = "<"
				elif(cartSymbol == "v"):
					cart[2] = ">"
				elif(cartSymbol == "<"):
					cart[2] = "^"
				elif(cartSymbol == ">"):
					cart[2] = "v"
				else:
					raise ERROR
			elif(nextChar == "+"):
				if(turn == 2):
					#Turn right
					if(cartSymbol == "^"):
						cart[2] = ">"
					elif(cartSymbol == ">"):
						cart[2] = "v"
					elif(cartSymbol == "v"):
						cart[2] = "<"
					elif(cartSymbol == "<"):
						cart[2] = "^"
					else:
						raise ERROR
				elif(turn == 0):
					#Turn left
					if(cartSymbol == "^"):
						cart[2] = "<"
					elif(cartSymbol == ">"):
						cart[2] = "^"
					elif(cartSymbol == "v"):
						cart[2] = ">"
					elif(cartSymbol == "<"):
						cart[2] = "v"
					else:
						raise ERROR	
				cart[3] = (turn+1) % 3
			elif(nextChar in "-|"):
				pass
			else:
				print("unknown next character '",nextChar,"'")
				print(cart)
				raise ERROR
				
					
			#Move the cart, update the position dict
			cart[0] = (newX,newY)
			for x in range(len(carts)):
				if(carts[x][0] == cart[0] and cartID != carts[x][1] and not carts[x][4]):
					cart[4] = True
					carts[x][4] = True
					print(cart[1],"crashed into",carts[x][1],"at",cart[0],carts[x][0],"on tick",tick)
			
					#See if only 1 non-crashed cart remains
					crashSum = 0
					lastCart = None
					for x in carts:
						if(not x[4]):
							lastCart = x
						else:
							crashSum += 1
					print(crashSum,"are crashed out of",len(carts))
					if(crashSum+1 == len(carts)):
						print("Only cart left at Tick",tick)
						print(lastCart)
						return lastCart[0]
					break
		if(tick % 10000 == 0):
			print("Tick:",tick)
		tick += 1
		#showTrack(arr,carts)
		#input()
def showTrack(arr,carts):
	pos = {}
	for c in carts:
		pos[c[0]] = c[2]
	for y in range(len(arr)):
		for x in range(len(arr[y])):
			if((x,y) in pos):
				print(pos[(x,y)],end="")
			else:
				print(arr[y][x],end="")
		print()
			
		
def main():
	data = open("input.txt","r").read().split("\n")
	for i in range(len(data)):
		data[i] = list(data[i])
	print(findFirstCollision(data))
	
if __name__ == "__main__":
	main()