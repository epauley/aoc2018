inp = open("input","r").read().strip().split("\n")

points = []
for l in inp:
    posA = int(l.split("<")[1].split(",")[0])
    posB = int(l.split(", ")[1].split(">")[0])
    
    velA = int(l.split("<")[2].split(",")[0])
    velB = int(l.split(",")[2].split(">")[0])
    points.append([posA,posB,velA,velB])
for p in points:
    print(p)
#sys.exit(1)
#input()
message = 0
time = 0
while True:
    
    
    maxX = max(points,key = lambda k: k[0])[0]
    minX = min(points,key = lambda k: k[0])[0]
    maxY = max(points,key = lambda k: k[1])[1]
    minY = min(points,key = lambda k: k[1])[1]
    
    #print(maxX-minX,maxY-minY)
    if(maxY - minY == 9):
        pDict = {}
        for p in points:
            pDict[(p[0],p[1])] = True
        #Show
        pWrote = 0
        print(str(maxX-minX) + " " + str(maxY-minY))
        
        with open("message" + str(message) + ".txt","w") as f:
            
            for y in range(minY-10,maxY+10):
                for x in range(minX-10,maxX+10):
                    if((x,y) in pDict):
                        f.write("#")
                        pWrote += 1
                    else:
                        f.write(" ")
                f.write("\n")
            print("WROTE MESSAGE " + str(message))
        message += 1
        
        import sys
        print(time)
        sys.exit(0)
        
    #Inc
    for p in points:
        p[0] += p[2]
        p[1] += p[3]
    time += 1
    