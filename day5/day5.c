#include <stdio.h>
#include <stdlib.h>
#define MAXINPUT 60000

int collide(char* input,int size,char skip) {
	char stack[MAXINPUT];
	stack[0] = 0;
	unsigned int k = 0;
	unsigned int i = 0;
	while(i < size && ((input[i] >= 97 && input[i] <=97+25)||(input[i] >=65 && input[i] <= 90))) {
		if((input[i] | 32) == skip) {
			i += 1;
			continue;
		}

		if(k == 0 || ((input[i] ^ 32) != stack[k])) {
			k += 1;
			stack[k] = input[i];
		} else {
			k -= 1;	
		}
		i += 1;
	}
	return k;
}
int main() {
	char input[MAXINPUT];
	unsigned int length;

	FILE* fp = fopen("input","rb");
	fseek(fp,0,SEEK_END);
	length = ftell(fp);
	fseek(fp,0,SEEK_SET);
	printf("input is %d chars long\n",length);

	fread(input,1,length,fp);
	printf("part 1: %d\n",collide(input,length,0));

	unsigned int min = MAXINPUT;
	for(int i = 0;i<26;i++) {
		unsigned int result = collide(input,length,97+i);
		printf("%c: %d\n",97+i,result);
		if(result < min) min = result;
	}
	printf("part 2: %d\n",min);
}
