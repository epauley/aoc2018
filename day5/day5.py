inp = open("input","r").read().strip()

def stackBased(inp):
	stack = [None]
	for c in inp:
		lastChar = stack[-1]
		if(c.swapcase() == lastChar):
			stack.pop()
		else:
			stack.append(c)
	return len(stack)-1

print("Part 1")
print(stackBased(inp))

print("Part 2")
results = []
for i in range(26):
	char = chr(ord("a") + i)
	newInp = inp.replace(char,"").replace(char.upper(),"")
	size = stackBased(newInp)
	#print("size of",char,"=",size) 
	results.append([size,char])
print(min(results))
