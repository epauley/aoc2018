inp = open("input.txt").read().strip().split("\n")
xDict = {}
yDict = {}
maxX = -1
maxY = -1
minX = float("INF")
minY = float("INF")
for line in inp:
    
    if(line.startswith("x")):
        xVal = int(line.split("x=")[1].split(",")[0])
        maxX = max(xVal,maxX)
        minX = min(xVal,minX)
        ystart,yend = (int(y) for y in line.split("y=")[1].split(".."))
        maxY = max(yend,maxY)
        minY = min(ystart,minY)
        #print("x",xVal,ystart,yend)
        xDict[xVal] = xDict.get(xVal,[]) + [(ystart,yend)]
    elif(line.startswith("y")):
        yVal = int(line.split("y=")[1].split(",")[0])
        maxY = max(yVal,maxY)
        minY = min(yVal,minY)
        xstart,xend = (int(x) for x in line.split("x=")[1].split(".."))
        maxX = max(xend,maxX)
        minX = min(xstart,minX)
        yDict[yVal] = yDict.get(yVal,[]) + [(xstart,xend)]
        #print("y",yVal,xstart,xend)
        
print("Grid is from x:{0:" + str(maxX) + "} y{0:" + str(maxY) + "}")

def isBlock(x,y):
    if(x in xDict):
        for yStart,yEnd in xDict[x]:
            if(yStart <= y <= yEnd):
                return True
    if(y in yDict):
        for xStart,xEnd in yDict[y]:
            if(xStart <= x <= xEnd):
                return True
    return False
    
def printSol(water):
    for y in range(maxY+5):
        for x in range(minX,maxX+1):
            if(isBlock(x,y) and (x,y) in water):
                print("Block water collision",x,y,water[(x,y)])
            if(isBlock(x,y)):
                print("#",end="")
            elif((x,y) in water):
                t = water[(x,y)]
                if(t == "Settle"):
                    print("~",end="")
                elif(t == "Down" or t == "Flow"):
                    print("|",end="")
            else:
                print(".",end="")
        print()

def waterKernel():
    #Executes an action, receives a command, repeats until no actions are left
    water = {}
    actions = [(500,0,"Drop")]
    
    while actions:
        x,y,action = actions.pop()
        #print(x,y,action)
        if(action == "Drop"):
            actions += waterDrop(x,y,xDict,yDict,water,maxY)
        elif(action == "Spread"):
            actions += waterSpread(x,y,xDict,yDict,water,maxY)
        else:
            print(x,y,action)
            raise Error("UKNOWN ACTION")
        #print("Action List",actions)
        
    printSol(water)
    print("There are",len(water)-1,"items in water")
    print("yMin is",minY)
    print("Part 1:",len(water)-minY)
    settleSum = 0
    for k,v in water.items():
        if(v == "Settle"):
            settleSum += 1
    print("Part 2:",settleSum)
def waterDrop(x,y,xDict,yDict,water,maxY):
    #water[(x,y)] = "Drop"
    if(y > maxY):
        return [] 
    while True:
        #Is this spot already occupied?
        if((x,y) in water):
            waterType = water[(x,y)]
            #If we hit another block of moving water, stop
            if(waterType == "Down" or waterType == "Flow"):
                return []
            elif(waterType == "Settle"):
                #Perform a waterspread on the layer above
                return [(x,y-1,"Spread")]
                
        #while the space below is open, do a simple drop
        if(not isBlock(x,y)):
            water[(x,y)] = "Down"
            y += 1
        else:
            #water[(x,y)] = "Flow"
            #We have hit a block, start spreading here
            return [(x,y-1,"Spread")]
        if(y > maxY):
            return [] 
           
def waterSpread(x,y,xDict,yDict,water,maxY):
    #Spreads water left and right until it can fall, or hits a wall
    leftBound = x
    rightBound = x
    leftDrop = False
    rightDrop = False
    water[(x,y)] = "Flow"
    #Expand left bound
    while(True):
        #Check if we can move left
        if(isBlock(leftBound-1,y)):
            break
            
        #If we can move down as well,set left drop and stop
        if(not isBlock(leftBound-1,y+1) and water.get((leftBound-1,y+1),"Flow") != "Settle"):
            leftDrop = True
            break
        else:
            
            #There is something blocking downward movement, continue spreading
            leftBound -= 1
            
    #Expand right bound
    while(True):
        #Check if we can move right
        if(isBlock(rightBound+1,y)):
            break
            
        #If we can move down as well,set right drop and stop
        if(not isBlock(rightBound+1,y+1) and water.get((rightBound+1,y+1),"Flow") != "Settle"):
            rightDrop = True
            break
        else:
            #There is something blocking downward movement, continue spreading
            rightBound += 1
            
    actions = []
    
    if((not leftDrop) and (not rightDrop)):
        #blocked on both sides, settle this layer
        for xr in range(leftBound,rightBound+1):
            water[(xr,y)] = "Settle"
        #Spread above this
        actions.append([x,y-1,"Spread"])
    else:
        #We have a flow
        for xr in range(leftBound,rightBound+1):
            water[(xr,y)] = "Flow"
        if(leftDrop):
            actions.append([leftBound-1,y,"Drop"])
        if(rightDrop):
            actions.append([rightBound+1,y,"Drop"])
    return actions
waterKernel()
    
    